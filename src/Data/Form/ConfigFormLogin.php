<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/01/2015
 * Time: 15:51
 */

namespace Skimia\Config\Data\Form;

use Skimia\Config\Form\ConfigForm;
class ConfigFormLogin extends ConfigForm{

    protected $id = 'os.backend_login';
    protected $name = 'Ecran de connexion';
    protected $description = 'Customiser l\'écran de connexion comme le nom et les couleurs';
    protected $icon = 'mdi-communication-vpn-key';
    protected $saveSuccessMessage = "Mise à jour de l'écran de connexion vous pourrez apprécier les changements à la prochaine connexion";
    protected $template = 'skimia.config::forms.login';

    protected $configs = [
        'skimia.backend::general.url'=>['label'=>'Url de l\'écran de connexion','type'=>'text',
            '__alias'=>'url'],
        'skimia.backend::general.name' => ['label'=>'Titre du back-end','type'=>'text',
            '__alias'=>'name'],
        'skimia.backend::general.login.brand'=>['label'=>'Nom de la société','type'=>'text',
            '__alias'=>'brand'],
        'skimia.backend::general.login.title'=>['label'=>'Slogan','type'=>'text',
            '__alias'=>'title'],
        'skimia.backend::general.login.button'=>['label'=>'Bouton de Connexion','type'=>'text',
            '__alias'=>'button'],
        'skimia.backend::general.login.colors.main'=>['label'=>'Couleur Principale','type'=>'text',
            '__alias'=>'prim'],
        'skimia.backend::general.login.colors.second'=>['label'=>'Couleur Secondaire','type'=>'text',
            '__alias'=>'second'],
    ];
}
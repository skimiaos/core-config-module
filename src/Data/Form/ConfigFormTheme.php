<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/01/2015
 * Time: 15:51
 */

namespace Skimia\Config\Data\Form;

use Skimia\Config\Form\ConfigForm;
class ConfigFormTheme extends ConfigForm{

    protected $id = 'os.front_end_theme';
    protected $aclAction = 'change_theme';
    protected $name = 'Theme Front Office';
    protected $description = 'Aperçu des thèmes installés et sélection du thème actif.';
    protected $icon = 'mdi-image-color-lens';
    protected $saveSuccessMessage = "Mise a jour du theme et du jeu de couleurs Terminé. Vous pouvez réactualiser la page dés maintenant pour apprécier les changements";
    protected $template = 'skimia.config::forms.simple';

    protected $configs = [];

    protected function makeFields(){

        $this->configs = [
            'skimia.themes::theme.theme' => ['label'=>'Theme Front Office','type'=>'select',
                'choicesFct'=> function($field){

                    return $themes = \Theme::getThemes()->lists('label','name');
                }
            ],
            'skimia.themes::theme.assets'=>['label'=>'Jeu de couleurs','type'=>'select',
                'choicesFct'=> function($field){
                    return \Theme::getDefaultTheme()['assets'];
                }
            ],
        ];
        return parent::makeFields();
    }


    public function getOtherValues(){
        return [
            'themes'=>[
                \Theme::getThemes()
            ]
        ];
    }
}
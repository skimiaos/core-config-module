<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/01/2015
 * Time: 15:46
 */

namespace Skimia\Config\Form;



use UserConfig;
class UserConfigForm extends ConfigForm{


    protected $type = 'userconfig';
    protected $additional_info = 'Les configurations ci dessous sont spécifiques a votre utilisateur';

    protected function getOldData($name){
        return UserConfig::get($this->decode($name));
    }

    public function save(){
        global $app;
        if($this->isValid()){
            foreach($this->fields as $configKeyEcoded=>$formDefinition){
                $configKey = $this->decode($configKeyEcoded);
                $configValue = $this->$configKeyEcoded;
                $app['userconfigwriter']->write($configKey,\AngularFormHelper::TransformViewToData($formDefinition['type'],$configValue));
            }
        }
    }



}
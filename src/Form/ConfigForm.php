<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/01/2015
 * Time: 15:46
 */

namespace Skimia\Config\Form;

use Skimia\Angular\Form\AngularTrait;
use Skimia\Auth\Traits\Acl;
use Skimia\Form\Base\Form;
use Config;
class ConfigForm extends Form{

    use AngularTrait;
    use Acl;
    protected $aclAction = false;
    protected $id = null;
    protected $type = 'config';
    protected $name = '';
    protected $description = '';
    protected $saveBtn = 'Valider';
    protected $saveUrl = '';
    protected $saveSuccessMessage = 'Modifications effectuées avec succès!';
    protected $icon;

    protected $template = 'skimia.config::forms.simple';

    protected $configs = [];


    protected function getOldData($name){
        return Config::get($this->decode($name));
    }

    protected function makeFields(){

        foreach($this->configs as $configKey=>$formDefinition){
            $formDefinition = array_merge($formDefinition,['required'=>true]);
            $this->fields->put($this->encode($configKey),$formDefinition);
        }
        return $this;
    }

    public function getInitialisationData(){
        return ['configs'=>$this->fields,'page'=>$this->getDescriptionData()];
    }

    public function getValues(){

        return $this->getAllOldData();
    }
    public function getDescriptionData(){
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'description'=>$this->description,
            'icon'=>$this->icon
        ];
    }

    public function save(){
        global $app;
        if($this->isValid()){
            foreach($this->fields as $configKeyEcoded=>$formDefinition){
                $configKey = $this->decode($configKeyEcoded);
                $configValue = $this->$configKeyEcoded;
                $app['configwriter']->write($configKey,\AngularFormHelper::TransformViewToData($formDefinition['type'],$configValue));
            }
        }
    }

    public function getButtonSaveText(){
        return $this->saveBtn;
    }

    public function setSaveUrl($url){
        $this->saveUrl = $url;
    }
    public function getSaveUrl(){
        return $this->saveUrl;
    }
    public function getSaveSuccessMessage(){
        return $this->saveSuccessMessage;
    }
    public function encode($name){
        return str_replace(['.','/',':'],['__','--','$$'],$name);
    }
    public function decode($name){
        return str_replace(['__','--','$$'],['.','/',':'],$name);
    }

    public function getType(){
        return $this->type;
    }

    public function getOtherValues(){
        return false;
    }

    public function canExecute(){

        if($this->aclAction !== false && !empty($this->aclAction))
            return $this->getAcl()->can($this->aclAction);
        else
            return true;
    }

}
<?php

namespace Skimia\Config\Managers;

use BackendNavigation as Nav;
use Illuminate\Support\Collection;

use Event;
use Skimia\Angular\Facades\Angular;
use Skimia\Angular\Form\AngularTrait;
use Skimia\Auth\Traits\Acl;
use Skimia\Config\Form\ConfigForm;

class ConfigPanel{

    use Acl;

    protected $categories;

    public function __construct(){
        $this->categories = new Collection();

        return $this;
    }

    public function addCategory($key, $name,$icon = false){
        if($icon !== false){
            $icon = $icon;
        }
        $c = new Collection();
        if($this->categories->has($key)){
            $c = $this->categories->get($key)['pages'];
        }

        $this->categories->put($key,[
            'name'=>$name,
            'icon'=>$icon,
            'pages' => $c
        ]);
        return $this;
    }


    public function register(ConfigForm $configForm){
        $dData = new Collection($configForm->getDescriptionData());

        list($category,$page) = array_pad(explode('.',$dData['id'],2),2,null);

        if(!$this->categories->has($category))
            throw new \Exception('Category['.$category.'] N\'existe pas dans le Panneau de configuration' );


        $app = Angular::get(OS_APPLICATION_NAME);
        $app->addState('config.'.$category.'*'.$page,'/'.$category.'-'.$page,
            function($vars) use($configForm,$app){

                $app->isSecure();

                return $configForm->make(false,array_merge($vars,['angular_submit'=>'save(form,$event)','saveUrl'=>$configForm->getSaveUrl(),'saveBtn'=>$configForm->getButtonSaveText()]))->render();


            },
            function() use($configForm,$app){
                $app->isSecure();
                return \AResponse::r(array_merge($configForm->getDescriptionData(),['form'=>$configForm->getValues(),'other'=>$configForm->getOtherValues()]));
            });

        \Route::post('/backend/config/'.$category.'-'.$page.'/save',['as' => 'config::save.'.$category.'-'.$page,'uses'=> function() use($configForm,$app){
            $app->isSecure();
            if($configForm->isValid()){
                $configForm->save();
                return \AResponse::addMessage($configForm->getSaveSuccessMessage())->r();
            }
            \AResponse::addMessage('Erreur dans la sauvegarde','danger');
            return \AResponse::r($configForm->getInitialisationData());
        }]);
        $dData['ConfigForm'] = $configForm;
        $dData['post_url'] = route('config::save.'.$category.'-'.$page);
        $dData['state'] = 'config.'.$category.'*'.$page;
        $dData['type'] = $configForm->getType();
        $configForm->setSaveUrl($dData['post_url']);
        $this->categories->get($category)['pages']->put($page,$dData);

    }

    public function registerState($state, $category, $name, $description = '...', $icon = false,$aclAction = false){
        $page = [
            'id'=>str_replace('.','*',$state),
            'name'=>$name,
            'state'=>$state,
            'description'=>$description,
            'icon'=>$icon,
            'type'=>'state'
        ];

        if($aclAction)
            $page['aclAction'] = $aclAction;

        $this->categories->get($category)['pages']->put($name,$page);
    }

    public function getList(){
        $cates = [];
        //dd($this->categories);
        foreach($this->categories as $i=>$category){

            if(count($category['pages']) > 0){

                $cat = [];
                foreach($category['pages'] as $index=>$page){

                    if($page['type'] == 'state'){

                        //S'il existe on le teste sinon c'est que c'est free Party sur ce config form
                        if(
                            (isset($page['aclAction']) && !empty($page['aclAction']) && $this->getAcl()->can($page['aclAction']))
                            || !isset($page['aclAction'])
                            || empty($page['aclAction'])
                        ) {


                            $cat[$index] = $page;


                        }
                    }else{
                        if($page['ConfigForm']->canExecute())
                            $cat[$index] = $page;
                    }


                    if(isset($cat[$index]['ConfigForm']))
                            unset($cat[$index]['ConfigForm']);

                }

                if(count($cat) > 0)
                    $cates[$i] = array_merge($category,['pages'=>$cat]);

            }

        }
        return ['categories'=>$cates];
    }
    /*
    public function addSimplePage($key, $name,$configurations = [], $description = '...', $icon=false){
        $allowed_configs = [];
        $c = $configurations;
        foreach($c as $k => $v){
            $allowed_configs[] = $k;
            $configurations[$this->encode($k)] = array_merge(['name'=>$this->encode($k)],$v);
            unset($configurations[$k]);
        }
        return $this->addPage('simple',$key,$name,$allowed_configs,$configurations,$description,$icon);
    }

    public function addPage($type, $key, $name,$allowed_configs = [],$configs = [], $description = '...', $icon=false){
        list($section,$page) = array_pad(explode('.',$key,2),2,null);
        $config = new Collection();

        $config->put('name',$name);
        $config->put('type',$type);
        $config->put('key',$key);
        $config->put('description',$description);

        if($icon !== false){
            $icon = 'fa fa-'.$icon;
        }

        $config->put('icon',$icon);
        $config->put('config',$allowed_configs);
        $config->put('page', $configs);

        if($this->sections->has($section))
            $this->sections->get($section)['pages']->put($page,$config);
        else
            $this->pages->put($key,$config);
        return $this;
    }

    public function getConfigs(){
        if(!$this->event){
            Event::fire('backend.config.register');
            $this->event = true;
        }

        return [
            'sections'=>$this->sections,
            'pages'=>$this->pages
        ];
    }

    public function getPage($key){
        if(!$this->event){
            Event::fire('backend.config.register');
            $this->event = true;
        }

        list($section,$page) = array_pad(explode('.',$key,2),2,null);

        if($this->sections->has($section))
            return $this->sections->get($section)['pages']->get($page);
        else
           return $this->pages->get($key);
    }
*/
} 
<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 02/03/2015
 * Time: 10:17
 */

namespace Skimia\Config\Managers;
use Auth;
use Illuminate\Config\Repository;

class UserConfig extends Repository{

    public function getUser(){
        return Auth::user() == null ? 'guest':Auth::user()->id;
    }

    protected function getNewKey($key){
        $segments = explode('::',$key);

        $new_key = 'users/'.$this->getUser() .'/'.last($segments);
        if(count($segments)>1){
            $new_key =$segments[0].'::'. $new_key;
        }
        return $new_key;
    }

    protected function getPublicNewKey($key){
        $segments = explode('::',$key);

        $new_key = 'users/'.last($segments);
        if(count($segments)>1){
            $new_key =$segments[0].'::'. $new_key;
        }
        return $new_key;
    }

    public function has($key){

    }

    public function get($key, $default = null){



        $new_key = $this->getNewKey($key);
        $public_new_key = $this->getPublicNewKey($key);

        $value = parent::get($new_key,$default);
        if($value !== $default)
            return $value;

        $value = parent::get($public_new_key,$default);
        if($value !== $default)
            return $value;

        $value = parent::get($key,$default);
        if($value !== $default)
            return $value;
        else
            return $default;

    }


    public function set($key, $value){

    }

    protected function parseNamespacedSegments($key)
    {

        list($namespace, $item) = explode('::', $key);
        // First we'll just explode the first segment to get the namespace and group
        // since the item should be in the remaining segments. Once we have these
        // two pieces of data we can proceed with parsing out the item's value.
        $itemSegments = explode('.', $item);

        $groupAndItem = array_slice($this->parseBasicSegments($itemSegments), 1);

        return array_merge(array($namespace), $groupAndItem);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 16/11/2014
 * Time: 09:47
 */

namespace Skimia\Config;

use Illuminate\Config\LoaderInterface;
use Illuminate\Config\FileLoader;
class UserFileWriter extends FileWriter
{

    protected function getPath($environment, $group, $item, $namespace = null)
    {
        $this->hints = $this->loader->getNamespaces();
        $path = null;
        if (is_null($namespace)) {
            $path = $this->defaultPath;
        }
        elseif (isset($this->hints[$namespace])) {
            if($this->files->exists($this->hints[$namespace]."/users/")) {
                $path = $this->defaultPath . '/' . "packages/" . str_replace('.', '/', $namespace) . "/users/" . \UserConfig::getUser();
                if (!$this->files->exists($path)) {
                    $this->files->copyDirectory($this->hints[$namespace]."/users/", $path);
                }
            }
        }
        if (is_null($path))
            return null;
        $file = "{$path}/{$group}.php";
        if ( $this->files->exists($file) &&
            $this->hasKey($file, $item)
        )
            return $file;
        $file = "{$path}/{$group}.php";
        if ($this->files->exists($file))
            return $file;
        return null;
    }

}
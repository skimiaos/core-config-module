<?php

namespace Skimia\Config;

use Blade;
use Illuminate\Filesystem\Filesystem;
use Orchestra\Support\Facades\App;
use Skimia\Config\Managers\UserConfig;
use Skimia\Modules\ModuleBase;

class Module extends ModuleBase{

    public function preBoot(){

    }

    public function beforeRegisterModules(){

        $loader = null;
        $this->app->bind('Skimia\Config\Repository', function($app) use ($loader)
        {
            if(!isset($loader))
                $loader = \Config::getLoader();

            $writer = new FileWriter($loader, $app['path'].'/config');
            return new Repository($loader, $writer, $app['env']);
        });

        $this->app['configwriter'] = $this->app->share(function($app)
        {
            return $app->make('Skimia\Config\Repository');
        });

        $this->app->bind('Skimia\Config\UserRepository', function($app) use ($loader)
        {
            if(!isset($loader))
                $loader = \Config::getLoader();

            $writer = new UserFileWriter($loader, $app['path'].'/config');
            return new UserRepository($loader, $writer, $app['env']);
        });

        $this->app->bind('Skimia\Config\Managers\UserConfig', function($app) use ($loader)
        {
            $loader =  new UserFileLoader(new Filesystem, $app['path'].'/config');

            return new UserConfig($loader, $app['env']);
        });

        $this->app['userconfigwriter'] = $this->app->share(function($app)
        {
            return $app->make('Skimia\Config\UserRepository');
        });

        \Event::listen('skimia.modules::module_base.init.config',function(ModuleBase $module,$conf_path){
            if(isset($this->app['configwriter'])){
                $this->app['configwriter']->package($module->getCanonical(), $conf_path, $module->getCanonical('.'));
            }
        });
    }


    public function getAliases(){
        return [
            'ConfigPanel' => 'Skimia\Config\Facades\ConfigPanel',
            'UserConfig' => 'Skimia\Config\Facades\UserConfig',
        ];
    }
} 
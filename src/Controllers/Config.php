<?php

namespace Skimia\Config\Controllers;

use Angular;
use Controller;
use AResponse;
use Input;
use Config as Conf;
use Skimia\Config\Facades\ConfigPanel;

class Config extends Controller{

    public function index(){

        return AResponse::r(ConfigPanel::getList());

    }

} 
<?php

namespace Skimia\Config\Facades;

use \Illuminate\Support\Facades\Facade;

class UserConfig extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Config\Managers\UserConfig';
    }
}
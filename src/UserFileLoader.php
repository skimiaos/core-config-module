<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 16/11/2014
 * Time: 09:47
 */

namespace Skimia\Config;

use Illuminate\Config\LoaderInterface;
use Illuminate\Config\FileLoader;
class UserFileLoader extends FileLoader
{
    public function load($environment, $group, $namespace = null)
    {
        $items = array();

        // First we'll get the root configuration path for the environment which is
        // where all of the configuration files live for that namespace, as well
        // as any environment folders with their specific configuration items.
        $path = $this->getPath($namespace);

        if (is_null($path))
        {
            return $items;
        }

        // First we'll get the main configuration file for the groups. Once we have
        // that we can check for any environment specific files, which will get
        // merged on top of the main arrays to make the environments cascade.
        $file = "{$path}/{$group}.php";

        if ($this->files->exists($file))
        {
            $items = $this->getRequire($file);
        }

        // Finally we're ready to check for the environment specific configuration
        // file which will be merged on top of the main arrays so that they get
        // precedence over them if we are currently in an environments setup.
        $file = "{$path}/{$environment}/{$group}.php";

        if ($this->files->exists($file))
        {
            $items = $this->mergeEnvironment($items, $file);
        }

        return $items;
    }

    protected function getPath($namespace)
    {
        $this->hints = \Config::getLoader()->getNamespaces();
        $path = null;
        if (is_null($namespace)) {
            $path = $this->defaultPath;
        }
        elseif (isset($this->hints[$namespace])) {
            if($this->files->exists($this->hints[$namespace]."/users/")) {
                $path = $this->defaultPath . '/' . "packages/" . str_replace('.', '/', $namespace) . "/users/" . \UserConfig::getUser();
                if (!$this->files->exists($path)) {
                    $this->files->copyDirectory($this->hints[$namespace]."/users/", $path);
                }
            }
        }
        if (is_null($path))
            return null;
        return $path;
    }

}
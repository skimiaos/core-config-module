<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 16/11/2014
 * Time: 09:45
 */

namespace Skimia\Config;

use Illuminate\Config\LoaderInterface;
use Illuminate\Config\Repository as RepositoryBase;
class UserRepository extends RepositoryBase
{
    /**
     * The config rewriter object.
     *
     * @var string
     */
    protected $writer;
    /**
     * Create a new configuration repository.
     *
     * @param  \Illuminate\Config\LoaderInterface  $loader
     * @param  string  $environment
     * @return void
     */
    public function __construct(LoaderInterface $loader, $writer, $environment)
    {
        $this->writer = $writer;
        parent::__construct($loader, $environment);
    }
    /**
     * Write a given configuration value to file.
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function write($key, $value)
    {

        list($namespace, $group, $item) = $this->parseKey($key);
        $result = $this->writer->write($item, $value, $this->environment, $group, $namespace);

        if(!$result) throw new \Exception('File could not be written to');

        $this->set($key, $value);
    }

    protected function parseNamespacedSegments($key)
    {
        list($namespace, $item) = explode('::', $key);

        // First we'll just explode the first segment to get the namespace and group
        // since the item should be in the remaining segments. Once we have these
        // two pieces of data we can proceed with parsing out the item's value.
        $itemSegments = explode('.', $item);

        $groupAndItem = array_slice($this->parseBasicSegments($itemSegments), 1);

        return array_merge(array($namespace), $groupAndItem);
    }
} 
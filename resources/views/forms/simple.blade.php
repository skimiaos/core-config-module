{{-- Simple config page
  Implements blocks:
    - page.content (backend.layout.config-page)
  --}}

@extends('skimia.backend::layouts.config-page')

@block('page.actions')
<a class="waves-effect waves-light btn-flat transparent white-text"
   ng-click="save(form,$event,$event)">
    <i class="os-icon-floppy-1"></i>
    Enregistrer
</a>

@endblock
@block('page.content')

@block('form.submit')

@endoverride
    @include(Config::get('skimia.angular::templates.form'), array('fields'=>$fields,'form'=>$form,'angular_submit'=> $angular_submit,'saveBtn'=>$saveBtn))

@endoverride


@Controller
//<script>
    $scope.save = function($data,$event){
        var deferred = $q.defer();
        $angular_response($scope,$event).post('{{$saveUrl}}',$data).
                success(function(data, status, headers, config) {
                    deferred.resolve(data);
                });
        return deferred.promise;
    };

    controller_handler.notifyScopeInitialised = function($scope){
        $scope.name_desc.name = $scope.name;
        $scope.name_desc.description = $scope.description;
    }
//</script>
@EndController
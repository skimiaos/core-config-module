{{-- Login config page
  Implements blocks:
    - page.container.class (backend.layout.config-page)
    - page.content (backend.layout.config-page)
  --}}

@extends('skimia.backend::layouts.config-page')

@block('grid.container.class') config-form-login @endoverride
@block('page.actions')
<a class="waves-effect waves-light btn-flat transparent white-text"
   ng-click="save(form,$event,$event)">
    <i class="os-icon-floppy-1"></i>
    Enregistrer
</a>

@endblock
@block('page.content')

    @block('grid.cols.1')
        @block('form.content')
            {{ a_render_field('url',$fields) }}
            {{ a_render_field('name',$fields) }}
            {{ a_render_field('brand',$fields) }}
            {{ a_render_field('title',$fields) }}
            {{ a_render_field('button',$fields) }}
        @endoverride
        @block('form.submit')

        @endoverride
        @include(Config::get('skimia.angular::templates.form'), array('fields'=>$fields,'form'=>$form,'angular_submit'=> $angular_submit,'saveBtn'=>$saveBtn))
    @endoverride

    @block('grid.cols.2')
        <os-container direction="column" style="  position: absolute;top: 0;left: 0;right: 0;bottom: 0; ">
            <div os-flex="3" class="preview">
                <div class="second" style="background-color: {{ form.<?php echo a_field_id('second',$fields); ?> }}">
                    <p>Logo</p>
                </div>
                <div class="prim" style="background-color: {{ form.<?php echo a_field_id('prim',$fields); ?> }}">
                    <p>Formulaire de Connexion</p>
                </div>
            </div>
            <div>
                <div class="col m6">
                    {{ a_render_field('prim',$fields) }}
                </div>
                <div class="col m6">
                    {{ a_render_field('second',$fields) }}
                </div>
                <div class="col m6">
                    <ng-color-picker selected='form.<?php echo a_field_id('prim',$fields); ?>'></ng-color-picker>
                </div>
                <div class="col m6">
                    <ng-color-picker selected='form.<?php echo a_field_id('second',$fields); ?>'></ng-color-picker>
                </div>
            </div>
        </os-container>
    @endoverride

    @include('skimia.backend::partials.grids.columns', ['cols' => ['2', '3']])

@endoverride

{{-- Angular controller --}}
@Controller
//<script>
    $scope.save = function($data,$event){
        var deferred = $q.defer();

        $angular_response($scope,$event).post('{{$saveUrl}}',$data).
            success(function(data, status, headers, config) {
                deferred.resolve(data);
            });
            return deferred.promise;
    };

    controller_handler.notifyScopeInitialised = function($scope){
        $scope.name_desc.name = $scope.name;
        $scope.name_desc.description = $scope.description;
    }
//</script>
@EndController
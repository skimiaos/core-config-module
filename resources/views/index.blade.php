{{-- Config homepage template
  Implements blocks:
    - page.sidenav (backend.layouts.default)
  --}}

{{-- Parent layout --}}
@extends('skimia.backend::layouts.activity-home')

{{-- Sidenav partial include --}}
@block('page.sidenav')
    @include('skimia.backend::partials.sidenav.menu')
@endoverride

{{-- Angular Controller --}}
@Controller
    $scope.name_desc = {};
    $scope.name_desc.name = 'Panneau de Configuration';
    $scope.name_desc.description = '';
@EndController


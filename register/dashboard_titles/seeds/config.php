<?php
Event::listen('skimia.backend::seed.dashboard.default.sections', function(){
    return ['configuration'=>'Configuration'];
});
Event::listen('skimia.backend::seed.dashboard.default.tiles', function($admin){
    if($admin){
        return ['configuration'=>[
            'config_index'=>[
                'static_id'=> 'config',
                'size'=>'large'
            ],
            'front_office_theme'=>[
                'static_id'=> 'config.front_end_theme',
                'size'=>'medium'
            ]
        ]];
    }else{
        return ['configuration'=>[
            'config_index'=>[
                'static_id'=> 'config',
                'size'=>'large'
            ]
        ]];
    }

},10000);
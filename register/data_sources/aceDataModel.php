<?php

Event::listen('skimia.angular::dataSources.provideDefaultSources:'.OS_APPLICATION_NAME,function(){
   $app = Angular::get(OS_APPLICATION_NAME);
    $app->provideDefaultSource('kvp','ace-editor',[
        'fontSize' => UserConfig::get('skimia.pages::editor.font_size'),
        'theme' => UserConfig::get('skimia.pages::editor.theme'),
        'showGutter' => UserConfig::get('skimia.pages::editor.show_gutter'),
        'showInvisibles' => UserConfig::get('skimia.pages::editor.show_invisibles'),
        'highlightActiveLine' => UserConfig::get('skimia.pages::editor.highlight_active_line'),
        'wrap' => UserConfig::get('skimia.pages::editor.word_wrap'),
        'tabSize' => UserConfig::get('skimia.pages::editor.tab_size'),
        'useSoftTabs' => !UserConfig::get('skimia.pages::editor.use_hard_tabs'),

    ]);
});
<?php

return [
    'name'        => 'Module Config',
    'author'      => 'Skimia',
    'description' => 'Undefined Description',
    'namespace'   => 'Skimia\\Config',
    'require'     => ['skimia.modules','skimia.blade','skimia.form','skimia.angular','skimia.auth','skimia.backend']
];